package com.n26.transactions.statistics.controllers;

import com.n26.TestApplication;
import com.n26.codechallenge.tranxstatistics.dtos.StatisticsDto;
import com.n26.codechallenge.tranxstatistics.dtos.StatisticsResponse;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import static io.restassured.RestAssured.given;

@EnableAutoConfiguration
public class StatisticsControllerTest extends TestApplication {

    @Before
    public void setup() {}

    @Test
    public void validateInitialStats() throws Exception {
        StatisticsResponse statisticsresp = given()
                .baseUri("http://localhost")
                .port(port)
                .contentType("application/json")
        .when()
                .get("/statistics")
        .then()
                .assertThat()
                .statusCode(200)
                .log().ifValidationFails()
        .and()
        .extract().body().as(StatisticsResponse.class);
        Assert.assertEquals(0.0, Double.valueOf(statisticsresp.getSum()).doubleValue(),0.0);
        Assert.assertEquals(0.0, Double.valueOf(statisticsresp.getAvg()).doubleValue(),0.0);
        Assert.assertEquals(0.0, Double.valueOf(statisticsresp.getMax()).doubleValue(),0.0);
        Assert.assertEquals(0.0, Double.valueOf(statisticsresp.getMin()).doubleValue(),0.0);
        Assert.assertEquals(0, statisticsresp.getCount(), 0.0);
        
    }
}