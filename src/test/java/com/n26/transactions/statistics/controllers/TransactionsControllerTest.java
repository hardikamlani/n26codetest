package com.n26.transactions.statistics.controllers;

import com.n26.codechallenge.tranxstatistics.dtos.TransactionReqObject;
import com.n26.codechallenge.tranxstatistics.dtos.TransactionsDto;
import com.n26.TestApplication;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static io.restassured.RestAssured.given;

@EnableAutoConfiguration
public class TransactionsControllerTest extends TestApplication {
	private static final DateTimeFormatter TIMESTAMP_FORMATTER = DateTimeFormatter
            .ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX").withZone(ZoneId.of("UTC"));

    @Before
    public void setup() {}

    @Test
    public void insertNewTransaction() throws Exception {
        given()
                .baseUri("http://localhost")
                .port(port)
                .contentType("application/json")
                .body(getNewTransaction())
        .when()
                .post("/transactions")
        .then()
                .assertThat()
                .statusCode(201);
    }

    private TransactionReqObject getNewTransaction() {
    	
    	Instant timestamp = Instant.now().plusMillis(0);
    	return new TransactionReqObject().withTimestamp(TIMESTAMP_FORMATTER.format(timestamp)).withAmount("90.00");
    }
}