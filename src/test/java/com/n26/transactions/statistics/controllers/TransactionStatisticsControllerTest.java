package com.n26.transactions.statistics.controllers;

import com.n26.codechallenge.tranxstatistics.dtos.StatisticsDto;
import com.n26.codechallenge.tranxstatistics.dtos.StatisticsResponse;
import com.n26.codechallenge.tranxstatistics.dtos.TransactionReqObject;
import com.n26.codechallenge.tranxstatistics.dtos.TransactionsDto;
import com.n26.TestApplication;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.stream.IntStream;

import static io.restassured.RestAssured.given;

@EnableAutoConfiguration
public class TransactionStatisticsControllerTest extends TestApplication {
	
	private static final DateTimeFormatter TIMESTAMP_FORMATTER = DateTimeFormatter
            .ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX").withZone(ZoneId.of("UTC"));


    @Before
    public void setup() {}

    @Test
    public void testSequentialInserts() throws Exception {
        addExpiredTransactions();
        getInitialStatistics();
        addValidTransactions();
        getFinalStatistics();
    }

    private void addExpiredTransactions() throws Exception {

        IntStream.range(0, 100).asDoubleStream().forEach(dAmount ->
                given()
                        .baseUri("http://localhost")
                        .port(port)
                        .contentType("application/json")
                        .body(getExpiredTransaction(dAmount))
                .when()
                        .post("/transactions")
                .then()
                        .assertThat()
                        .statusCode(204));
    }

    private void getInitialStatistics() throws Exception {

        StatisticsResponse statisticsResp =
                given()
                        .baseUri("http://localhost")
                        .port(port)
                        .contentType("application/json")
                .when()
                        .get("/statistics")
                .then()
                        .assertThat()
                        .statusCode(200)
                        .log().ifValidationFails()
                .and()
                        .extract().body().as(StatisticsResponse.class);

        Assert.assertEquals(90.00, Double.valueOf(statisticsResp.getSum()).doubleValue(),0.0);
        Assert.assertEquals(90.00, Double.valueOf(statisticsResp.getAvg()).doubleValue(),0.0);
        Assert.assertEquals(90.00, Double.valueOf(statisticsResp.getMax()).doubleValue(),0.0);
        Assert.assertEquals(90.00, Double.valueOf(statisticsResp.getMin()).doubleValue(),0.0);
        Assert.assertEquals(1, statisticsResp.getCount(), 0.0);
    }

    private void addValidTransactions() throws Exception {

        IntStream.range(0, 100).asDoubleStream().forEach(dAmount ->
                given()
                        .baseUri("http://localhost")
                        .port(port)
                        .contentType("application/json")
                        .body(getNewTransaction(dAmount))
                .when()
                        .post("/transactions")
                .then()
                        .assertThat()
                        .statusCode(201));
    }

    private void getFinalStatistics() throws Exception {

    	StatisticsResponse statisticsResp =
                given()
                        .baseUri("http://localhost")
                        .port(port)
                        .contentType("application/json")
                .when()
                        .get("/statistics")
                .then()
                        .assertThat()
                        .statusCode(200)
                        .log().body()
                .and()
                        .extract().body().as(StatisticsResponse.class);

        Assert.assertEquals(5040.00, Double.valueOf(statisticsResp.getSum()).doubleValue(),0.0);
        Assert.assertEquals(50.00, Double.valueOf(statisticsResp.getAvg()).doubleValue(), 0.5);
        Assert.assertEquals(99.00,Double.valueOf(statisticsResp.getMax()).doubleValue() ,0.0);
        Assert.assertEquals(0.00, Double.valueOf(statisticsResp.getMin()).doubleValue() ,0.0);
        Assert.assertEquals(101, statisticsResp.getCount(), 0.0);
    }
    
    private TransactionReqObject getNewTransaction(double dAmount) {
    	
    	Instant timestamp = Instant.now().plusMillis(-30000);
    	return new TransactionReqObject().withTimestamp(TIMESTAMP_FORMATTER.format(timestamp)).withAmount(String.valueOf(dAmount));
    }
    
   /* private TransactionsDto getNewTransaction(double dAmount) {
        return new TransactionsDto().withAmount(dAmount).withTimestamp((Instant.now().getEpochSecond() * 1000) - 30000);
    } */

    private TransactionReqObject getExpiredTransaction(double dAmount) {
    	Instant timestamp = Instant.now().plusMillis(-70000);
        return new TransactionReqObject().withAmount(String.valueOf(dAmount)).withTimestamp(TIMESTAMP_FORMATTER.format(timestamp));
    }
}