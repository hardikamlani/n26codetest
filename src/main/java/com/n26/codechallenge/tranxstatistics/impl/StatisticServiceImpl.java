package com.n26.codechallenge.tranxstatistics.impl;

import java.time.Instant;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.n26.codechallenge.tranxstatistics.dtos.StatisticsDto;
import com.n26.codechallenge.tranxstatistics.dtos.StatisticsResponse;
import com.n26.codechallenge.tranxstatistics.entity.StatisticsGenerator;
import com.n26.codechallenge.tranxstatistics.entity.TransactionsContainerImpl;
import com.n26.codechallenge.tranxstatistics.services.StatisticsService;


@Service
public class StatisticServiceImpl implements StatisticsService {
	
	private final TransactionsContainerImpl transactionsContainer;
	
	@Autowired
	public  StatisticServiceImpl(final TransactionsContainerImpl transactionsContainer){
		this.transactionsContainer = transactionsContainer;
	}

	public StatisticsDto getTransactionStatistics() {
		List<StatisticsGenerator> txnStatsAggregator = getStatisticsAggregators();
		
		StatisticsDto result = new StatisticsDto();
		
		txnStatsAggregator.forEach(t -> t.mergeToResult(result));
		
		// in case of reset correct the minimum and maximum value.
		transactionsContainer.minMaxCorrection(result);
		return result;
	}
	
	public StatisticsResponse getTransactionstatsResponse(StatisticsDto inputObject){
		StatisticsResponse responsObject = new StatisticsResponse();
		if(null!=inputObject){
			responsObject.setAvg(transactionsContainer.trailzeroappend(String.valueOf(inputObject.getAvg())));
			responsObject.setMax(transactionsContainer.trailzeroappend(String.valueOf(inputObject.getMax())));
			responsObject.setMin(transactionsContainer.trailzeroappend(String.valueOf(inputObject.getMin())));
			responsObject.setSum(transactionsContainer.trailzeroappend(String.valueOf(inputObject.getSum())));
			responsObject.setCount(inputObject.getCount());
		}
		return responsObject;
	}
	
	/**
	 * Get All relevant transaction statistics aggregtors
	 * @param timestamp the time stamp 
	 * @return a List of StatisticsGenerator
	 */
	private List<StatisticsGenerator> getStatisticsAggregators() {
		long currentTime = Instant.now().toEpochMilli();
		return transactionsContainer.getValidStatisticsGenerators(currentTime);		
	}
	

}
