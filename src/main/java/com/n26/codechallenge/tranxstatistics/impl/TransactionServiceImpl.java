package com.n26.codechallenge.tranxstatistics.impl;

import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.n26.codechallenge.tranxstatistics.dtos.TransactionReqObject;
import com.n26.codechallenge.tranxstatistics.dtos.TransactionsDto;
import com.n26.codechallenge.tranxstatistics.entity.TransactionsContainerImpl;
import com.n26.codechallenge.tranxstatistics.exceptions.TransactionNotInRangeException;
import com.n26.codechallenge.tranxstatistics.exceptions.UnprocessableEntityException;
import com.n26.codechallenge.tranxstatistics.services.TransactionsService;
import com.n26.codechallenge.tranxstatistics.utility.DateUtility;

@Service
public class TransactionServiceImpl implements TransactionsService{
	
	private final TransactionsContainerImpl transactionsContainer;
	
	@Autowired
	public  TransactionServiceImpl(final TransactionsContainerImpl transactionsContainer){
		this.transactionsContainer = transactionsContainer;
	}

	public void addTransaction(TransactionsDto transaction) throws TransactionNotInRangeException ,UnprocessableEntityException{
		long currentTimeStamp = Instant.now().toEpochMilli();
		transactionsContainer.insertTransaction(transaction, currentTimeStamp);
	}

	public void deleteTransactions() {
		transactionsContainer.clear();	
	}

	public TransactionsDto validateTransactionObject(
			TransactionReqObject transaction) {
		TransactionsDto responsObject = null;
		try{
			if(null!=transaction){
				responsObject = new TransactionsDto();
				if(transaction.getAmount()!=""){
					responsObject.setAmount(Double.valueOf(transaction.getAmount()));
				}
				
				if(transaction.getTimestamp()!=""){
					responsObject.setTimestamp(DateUtility.getTimeinMillisecs(transaction.getTimestamp()));
				}
			}
		}catch(Exception e){
			responsObject = null;
		}
		
		return responsObject;
	}

}
