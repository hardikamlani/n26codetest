package com.n26.codechallenge.tranxstatistics.exceptions;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ErrorInformation {

    
    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorInformation.class);

    
    String statusCd;

    
    String errorCd;

    
    String ttl;

    
    String msg;
    
    String stackMessage;

    
    String advc;

    
    public ErrorInformation() {
        
    }

    
    public String getStatusCode() {
        return statusCd;
    }

    
    public void setStatusCode(String statusCode) {
        this.statusCd = statusCode;
    }

    
    public String getErrorCode() {
        return errorCd;
    }

    
    public void setErrorCode(String errorCode) {
        this.errorCd = errorCode;
    }

    
    public String getTitle() {
        return ttl;
    }

    
    public void setTitle(String title) {
        this.ttl = title;
    }

    
    public String getMessage() {
        return msg;
    }

   
    public void setStackMessage(String stackMessage) {
        this.stackMessage = stackMessage;
    }

    
    public String getStackMessage() {
        return stackMessage;
    }

    
    public void setMessage(String message) {
        this.msg = message;
    }

    
    public String getAdvice() {
        return advc;
    }

    
    public void setAdvice(String advice) {
        this.advc = advice;
    }
}
