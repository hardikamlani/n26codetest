package com.n26.codechallenge.tranxstatistics.controllers;

import javax.validation.Valid;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.BeanUtils;

import com.n26.codechallenge.tranxstatistics.dtos.TransactionReqObject;
import com.n26.codechallenge.tranxstatistics.dtos.TransactionsDto;
import com.n26.codechallenge.tranxstatistics.exceptions.TransactionNotInRangeException;
import com.n26.codechallenge.tranxstatistics.exceptions.UnprocessableEntityException;
import com.n26.codechallenge.tranxstatistics.services.TransactionsService;
import com.n26.codechallenge.tranxstatistics.utility.DateUtility;

@RestController
@RequestMapping("/transactions")
public class TransactionsController {

	@Autowired
	private TransactionsService transactionsService;

	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionsController.class);

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<?> createTransaction(@RequestBody @Valid TransactionReqObject transactionsReqObject) {
    	
    	TransactionsDto transactionsDto =transactionsService.validateTransactionObject(transactionsReqObject);
    	
    	if(transactionsDto == null){
    		return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
    	}else{
    	
    		if(transactionsDto.getTimestamp() < DateUtility.getTimeToCompare()) {
    			return new ResponseEntity(HttpStatus.NO_CONTENT);
    		}
    	
    		try{
    			transactionsService.addTransaction(transactionsDto);
    			return new ResponseEntity(HttpStatus.CREATED);
    		}catch(TransactionNotInRangeException e){
    			return new ResponseEntity(HttpStatus.NO_CONTENT);
    		}catch(UnprocessableEntityException e){
    			return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
    		}
		
    	}
		
	}
    
    @RequestMapping(method=RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<?> deleteTransactions(){
    	LOGGER.info("Deleting all the transactions :::");
    	transactionsService.deleteTransactions();
    	return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}