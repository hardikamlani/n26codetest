package com.n26.codechallenge.tranxstatistics.controllers;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

import com.n26.codechallenge.tranxstatistics.dtos.StatisticsDto;
import com.n26.codechallenge.tranxstatistics.dtos.StatisticsResponse;
import com.n26.codechallenge.tranxstatistics.services.StatisticsService;

@RestController
@RequestMapping("/statistics")
public class StatisticsController {

    @Autowired
    private StatisticsService statisticsService;

    private static final Logger LOGGER = LoggerFactory.getLogger(StatisticsController.class);

   
    @RequestMapping(method = GET)
    public @ResponseBody StatisticsResponse getStatistics() {
        StatisticsDto statsDto = statisticsService.getTransactionStatistics();
        return statisticsService.getTransactionstatsResponse(statsDto);
    }
}
