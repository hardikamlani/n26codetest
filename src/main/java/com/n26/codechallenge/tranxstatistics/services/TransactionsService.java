package com.n26.codechallenge.tranxstatistics.services;

import com.n26.codechallenge.tranxstatistics.dtos.TransactionReqObject;
import com.n26.codechallenge.tranxstatistics.dtos.TransactionsDto;
import com.n26.codechallenge.tranxstatistics.exceptions.TransactionNotInRangeException;
import com.n26.codechallenge.tranxstatistics.exceptions.UnprocessableEntityException;

public interface TransactionsService {

    void addTransaction(TransactionsDto transaction) throws TransactionNotInRangeException,UnprocessableEntityException;
    
    void deleteTransactions();
    
    public TransactionsDto  validateTransactionObject(TransactionReqObject transaction);
}
