package com.n26.codechallenge.tranxstatistics.services;

import com.n26.codechallenge.tranxstatistics.dtos.StatisticsDto;
import com.n26.codechallenge.tranxstatistics.dtos.StatisticsResponse;

public interface StatisticsService {

    StatisticsDto getTransactionStatistics();
    public StatisticsResponse getTransactionstatsResponse(StatisticsDto inputObject);
}