package com.n26.codechallenge.tranxstatistics.entity;

import java.math.BigDecimal;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.n26.codechallenge.tranxstatistics.dtos.StatisticsDto;
import com.n26.codechallenge.tranxstatistics.dtos.TransactionsDto;

public class StatisticsGenerator {
	
	private ReadWriteLock lock;
	
	private StatisticsDto statisticsObj;
	
	private long timestamp;
	
	public StatisticsGenerator(){
		statisticsObj = new StatisticsDto();
		this.lock = new ReentrantReadWriteLock();
	}

	public ReadWriteLock getLock() {
		return lock;
	}
	
	public void create(TransactionsDto transaction){
		statisticsObj.setMin(transaction.getAmount());
		statisticsObj.setMax(transaction.getAmount());
		statisticsObj.setCount(1);
		statisticsObj.setAvg(transaction.getAmount());
		statisticsObj.setSum(transaction.getAmount());
		timestamp = transaction.getTimestamp();
	}
	
	/**
	 * This method is used to merge the existing transction object to output transactn object.
	 * @param result
	 */
	public void mergeToResult(StatisticsDto result) {
		try{
			getLock().readLock().lock();
			
			result.setSum(get2digitPrecision(result.getSum() + getstatisticObj().getSum()));
			result.setCount(result.getCount() + getstatisticObj().getCount());
			result.setAvg(get2digitPrecision(result.getSum() / result.getCount()));
			
			if (result.getMin() > getstatisticObj().getMin()){
				result.setMin(get2digitPrecision(getstatisticObj().getMin()));
			}
			if (result.getMax() < getstatisticObj().getMax()){
				result.setMax(get2digitPrecision(getstatisticObj().getMax()));
			}	
		}finally {
			getLock().readLock().unlock();
		}
	}
	
	
	
	
	/**
	 * this method will be called when the transction exist
	 * in that case newe transaction will be merged with existing one
	 * @param transaction
	 */
	public void mergetoExistingTranx(TransactionsDto transaction) {
		statisticsObj.setSum(statisticsObj.getSum() + transaction.getAmount());
		statisticsObj.setCount(statisticsObj.getCount() + 1);
		statisticsObj.setAvg(statisticsObj.getSum() / statisticsObj.getCount());
		
		if (statisticsObj.getMin() > transaction.getAmount()){
			statisticsObj.setMin(transaction.getAmount());
		}
		if (statisticsObj.getMax() < transaction.getAmount()){
			statisticsObj.setMax(transaction.getAmount());
		}
		
	}
	
	public boolean isEmpty(){
		return statisticsObj.getCount() == 0;
	}
	
	public long getTimestamp(){
		return timestamp;
	}
	
	public StatisticsDto getstatisticObj(){
		return statisticsObj;
	}

	public void reset() {
		statisticsObj.reset();	
		timestamp = 0;
	}
	
	public double get2digitPrecision(double dubValue){
		double returnValue;
		BigDecimal bdValue = new BigDecimal(dubValue);
		returnValue = bdValue.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		return returnValue;
	}
	
	

	
}
