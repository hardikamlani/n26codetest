package com.n26.codechallenge.tranxstatistics.entity;


import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.n26.codechallenge.tranxstatistics.dtos.StatisticsDto;
import com.n26.codechallenge.tranxstatistics.dtos.TransactionsDto;
import com.n26.codechallenge.tranxstatistics.exceptions.TransactionNotInRangeException;
import com.n26.codechallenge.tranxstatistics.exceptions.UnprocessableEntityException;

@Component
public class TransactionsContainerImpl implements TransactionsContainer{
	
	private StatisticsGenerator[] StatisticsGenerator;
	
	//max time (in millis) from current time that transaction will be considered valid 
	@Value("${time.milliseconds.maximum}")
	private int maxTimeMillsToKeep;

	//represents the time interval (e.g. 1000ms = one second)
	@Value("${time.milliseconds.interval}")
	private int timeMillsInterval;

    private TransactionsContainerImpl () {}
    
    @PostConstruct
    private void postConstruct(){
    	if (maxTimeMillsToKeep <= 0 || timeMillsInterval <= 0) 
    		throw new IllegalArgumentException("YML is missing valid positive values for time.mills.max or time.mills.interval");
    	this.StatisticsGenerator = new StatisticsGenerator[maxTimeMillsToKeep / timeMillsInterval];
    	initializeStatsPopulate();	
    }
    
    /**
     * Initialize the statistics populate logic 
     * by creating emmpty array of in equation with
     * the max transaction time and persec , min transctn time.
     */
    private void initializeStatsPopulate(){
    	//fill transactionStatistics with empty statistics aggregators
    	for (int x = 0; x < StatisticsGenerator.length; x++){
    		StatisticsGenerator[x] = new StatisticsGenerator();
    	}	
    }

    /**
     * This method is used to insert the transaction in the in memory DB.
     */
    public void insertTransaction(TransactionsDto transaction, long currentTimestamp) throws TransactionNotInRangeException,UnprocessableEntityException{   	
    	if (!checkifValidTranx(transaction.getTimestamp(), currentTimestamp)) 
    		throw new TransactionNotInRangeException();
    	    	
    	aggregate(transaction, currentTimestamp);	
    }
    /**
     * This method is used to get the valid transaciotn obejct by comparing 
     * the date and everyring.
     */
	public List<StatisticsGenerator> getValidStatisticsGenerators(long currentTimestamp) {
		
		return Arrays.stream(StatisticsGenerator).filter(txObject->checkifValidTranx(txObject.getTimestamp(), currentTimestamp)).collect(Collectors.toList());
	}
	/**
	 * This method is used to clear the complete transction container object
	 */
	public void clear() {
		initializeStatsPopulate();		
	}


	
	private void aggregate(TransactionsDto transaction, long currentTimestamp) throws UnprocessableEntityException {
		//getting the transaction index
		
		if((Instant.now().toEpochMilli()-transaction.getTimestamp())/ timeMillsInterval <0){ // this means it is a future date transaction
			throw new UnprocessableEntityException();
		}
		
		int index = getIndexofTransaction(transaction);
		
		StatisticsGenerator txnStatisticObject = StatisticsGenerator[index];

		try {
			txnStatisticObject.getLock().writeLock().lock();
			
			// in case aggregator is empty
			if (txnStatisticObject.isEmpty()) { 
				txnStatisticObject.create(transaction);

			} else {
				// check if existing aggregator is still valid
				if (checkifValidTranx(txnStatisticObject.getTimestamp(), currentTimestamp)) {
					txnStatisticObject.mergetoExistingTranx(transaction);
				} else { //if not valid
					txnStatisticObject.reset(); 
					txnStatisticObject.create(transaction);
				}
			}

		} finally {
			txnStatisticObject.getLock().writeLock().unlock();
		}
	}

	/**
	 * Get the correct transaction index in the StatisticsGenerator array
	 * @param transaction the transaction
	 * @return the index
	 */
	private int getIndexofTransaction(TransactionsDto transaction){
    	long txnTime = transaction.getTimestamp();
    	
    	long currTime = Instant.now().toEpochMilli();
    	
    	return (int)((currTime - txnTime) / timeMillsInterval) % (maxTimeMillsToKeep / timeMillsInterval);
    }
    

    /**
     * Check if time stamp is in range
     * @param timeStamp the txn timestamp
     * @param currentTimestamp the current timestamp
     * @return true if valid
     */
    private boolean checkifValidTranx(long txnTimeStamp, long currentTimestamp){
    	return txnTimeStamp >= currentTimestamp - maxTimeMillsToKeep;
    }
    
    public String trailzeroappend(String inputValue){
		String returnValue = inputValue;
		try{
			if(inputValue.contains(".")){
				String[] locl_arry = inputValue.split("\\.");
				if(locl_arry[1].length()== 1 && Integer.valueOf(locl_arry[1]).intValue() ==0){
					returnValue = locl_arry[0] + ".00";
				}else if(locl_arry[1].length()== 1 && (Integer.valueOf(locl_arry[1]).intValue() >=1 && Integer.valueOf(locl_arry[1]).intValue() <=9)){
					returnValue = locl_arry[0] + "."+ Integer.valueOf(locl_arry[1]).intValue() +"0";
				}
			}
		}catch(Exception e){
			returnValue = inputValue;
		}
		
		return returnValue;
	}
    
    public void minMaxCorrection(StatisticsDto statsDto){
    	
    	if(null!=statsDto){
    		if(statsDto.getMax() == Long.MIN_VALUE){
    			statsDto.setMax(0.0);
    		}
    		if(statsDto.getMin() == Long.MAX_VALUE){
    			statsDto.setMin(0.0);
    		}
    	}
    	
    }
}
