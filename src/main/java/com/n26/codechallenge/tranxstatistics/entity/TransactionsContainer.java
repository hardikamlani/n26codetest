package com.n26.codechallenge.tranxstatistics.entity;

import java.util.List;

import com.n26.codechallenge.tranxstatistics.dtos.TransactionsDto;
import com.n26.codechallenge.tranxstatistics.exceptions.TransactionNotInRangeException;
import com.n26.codechallenge.tranxstatistics.exceptions.UnprocessableEntityException;

public interface TransactionsContainer {
	
    /**
     * This method is used to add the transaction to inmemeroy DB.
     * @param transaction
     * @param timestamp
     * @throws TransactionNotInRangeException
     */
    void insertTransaction(TransactionsDto transaction, long timestamp) throws TransactionNotInRangeException,UnprocessableEntityException;
    
    /**
     * provide valid transactions from container for given timestamp
     * @param timestamp the timestamp
     * @return list of valid StatisticsGenerator from container
     */
   List<StatisticsGenerator> getValidStatisticsGenerators(long timestamp);
   
   /**
    * Clear container
    */
   void clear();
    
}
