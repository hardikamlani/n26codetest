package com.n26.codechallenge.tranxstatistics.dtos;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

public class TransactionReqObject  implements Serializable{
	private String timestamp;

    private String amount;

    private final static AtomicLong sequence = new AtomicLong(1);
    private final long id = sequence.getAndIncrement();

    public TransactionReqObject() {}

    public TransactionReqObject(String timestamp, String amount) {
        this.timestamp = timestamp;
        this.amount = amount;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public TransactionReqObject withTimestamp(String timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public String getAmount() {
        return this.amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public TransactionReqObject withAmount(String amount) {
        this.amount = amount;
        return this;
    }

    public long getId() {
        return this.id;
    }

	public String toString() {
		return "TransactionReqObject [timestamp=" + timestamp + ", amount="
				+ amount + "]";
	}

}
