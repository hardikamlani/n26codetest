package com.n26.codechallenge.tranxstatistics.utility;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.n26.codechallenge.tranxstatistics.exceptions.UnprocessableEntityException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.GregorianCalendar;
import java.util.TimeZone;

@Configuration
public class DateUtility {

    private static long milliSecondsToConsider;

    @Value("${statistics.milliseconds-to-consider:60000}")
    public void setMilliSecondsToConsider(long milliSeconds) {
        milliSecondsToConsider = milliSeconds;
    }

    public static long getTimeToCompare() {
    	 return (Instant.now().getEpochSecond() * 1000) - milliSecondsToConsider;
    }
    
    public static long getTimeinMillisecs(String inputTime) throws UnprocessableEntityException{
    	long returnValue = Long.MIN_VALUE;
    	try{
    	if(null!=inputTime && inputTime!=""){
    		String input = inputTime;
        	TimeZone utc = TimeZone.getTimeZone("UTC");
        	SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        	f.setTimeZone(utc);
        	GregorianCalendar cal = new GregorianCalendar(utc);
        	cal.setTime(f.parse(input));
        	returnValue = cal.getTimeInMillis();
        }
    	}catch(Exception e){
    		throw new UnprocessableEntityException();
    	}
    	return returnValue;
    	
    }
    
    
}
